package readability;
/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

/**
 * WordCounter will enter this state if the character is e.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.07
 */
public class EState extends State {

	@Override
	public void handleChar(WordCounter counter,char c) {
		if(isVowel(c) && !("Yy".indexOf(c) >= 0)) counter.setState(VOWELSTATE);
		else if(isE(c));
		else if(isLetter(c)) {
			counter.syllables++;
			counter.setState(CONSONANTSTATE);
		}
		else if(isDash(c)) counter.setState(DASHSTATE);
		else counter.setState(NONWORDSTATE);
	}

	@Override
	public void enterState(WordCounter counter) {
		
	}
	
}
