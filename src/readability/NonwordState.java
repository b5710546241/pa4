package readability;
/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

/**
 * WordCounter will enter this state if text is non-word.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.07
 */
public class NonwordState extends State {


	@Override
	public void handleChar(WordCounter counter,char c) {

	}

	@Override
	public void enterState(WordCounter counter) {
		
	}
	
}
