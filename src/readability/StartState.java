package readability;
/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

/**
 * WordCounter enter this state first.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.07
 */
public class StartState extends State {

	@Override
	public void handleChar(WordCounter counter,char c) {
		if(isE(c)) counter.setState(ESTATE);
		else if(isVowel(c)) counter.setState(VOWELSTATE);
		else if(isLetter(c)) counter.setState(CONSONANTSTATE);
		else if(isDash(c)) counter.setState(NONWORDSTATE);
		else counter.setState(NONWORDSTATE);
	}

	@Override
	public void enterState(WordCounter counter) {
		
	}
	
}
