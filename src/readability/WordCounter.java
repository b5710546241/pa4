package readability;
/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Counting the syllables of the word.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.07
 */
public class WordCounter {

	/** Condition for checking what kind of char it is. */
	private State state;

	/** Amount of sentence. */
	protected int sentences;

	/** Amount of syllables. */
	protected int syllables;

	/** Amount of words. */
	public int wordcount;

	/** Punctuation that end the sentence. */
	private String endSentence = ".;:?!";

	/** Status of word. */
	private boolean hasWord = false;

	/** Total amount of syllables. */
	private int totalSyllables = 0;

	/**
	 * Checking a letter.
	 * @param c is the character of the word
	 * @return true if the character is a letter, 
	 * 		   otherwise return false
	 */


	/**
	 * Setting a state of the character.
	 * @param newState is the state of the character
	 */
	public void setState(State newState) {
		if(newState.getClass() != state.getClass()) newState.enterState(this);
		this.state = newState;
	}



	/**
	 * Counting the syllables of the word. 
	 * @param word is a full word
	 * @return the amount of syllables
	 */
	public int countSyllable(String word) {
		syllables = 0;
		state = State.STARTSTATE;
		for(int k=0; k<word.length(); k++) {
			char c = word.charAt(k);
			if(endSentence.contains(Character.toString(c)) && hasWord){
				sentences++;
				hasWord = false;
				break;
			}

			state.handleChar(this,c);
			if(k==0 && state==State.DASHSTATE) break;
		}

		if (state==State.DASHSTATE) return 0;
		else if(state==State.ESTATE && syllables == 0) {
			syllables++;
		}

		if (syllables>0) {
			hasWord = true;
			wordcount++;
		}

		else hasWord = false;

		totalSyllables += syllables;

		return syllables;
	}

	/**
	 * Getting an amount of sentence.
	 * @return amount of sentences
	 */
	public int getSentences(){
		return sentences;
	}

	/**
	 * Checking a file name or an URL.
	 * Reading and counting word from file or URL.
	 * @param filename is the file name or the URL
	 */
	public void count(String filename){
		InputStream in = null;
		if(filename.contains("http://")) {
			try {
				URL url = new URL(filename);
				try {
					in = url.openStream();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
		else {
			File file = new File(filename);
			try {
				in = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		BufferedReader breader = new BufferedReader(new InputStreamReader(in));
		while(true) {
			String lineStr = null;
			try {
				lineStr = breader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(lineStr == null) break;
			String[] line = lineStr.split(" ");

			for(String word : line){
				word = word.replaceAll("[()\"]", "");
				syllables += countSyllable(word);
			}
		}
	}

	/**
	 * Getting readability description.
	 * @param countIndex is the amount of count index
	 * @return readability description
	 */
	public String getReadabilityDescription(double countIndex){

		if(countIndex>=100){
			return "4th grade student (elementary school)";
		}

		else if(countIndex>=90){
			return "5th grade student";
		}

		else if(countIndex>=80){
			return "6th grade student";
		}

		else if(countIndex>=70){
			return "7th grade student";
		}

		else if(countIndex>=65){
			return "8th grade student";
		}

		else if(countIndex>=60){
			return "9th grade student";
		}

		else if(countIndex>=50){
			return "High school student";
		}

		else if(countIndex>=30){
			return "College student";
		}

		else if(countIndex>=0){
			return "College graduate";
		}

		else if(countIndex<0){
			return "Advanced degree graduate";
		}

		return "";

	}

	/**
	 * 
	 * @param filename is a file name or an URL
	 * @return the result of wordcounter
	 */
	public String getResult(String filename){
		String line1 = String.format("Filename:\t\t%s\n", filename);
		String line2 = String.format("Number of Syllables:\t%,d   \n", syllables);
		String line3 = String.format("Number of Words:\t%,d       \n", wordcount);
		String line4 = String.format("Number of Sentences:\t%,d   \n", sentences);
		String line5 = String.format("Flesch Index:\t\t%.2f         \n", getCountIndex());
		String line6 = String.format("Readability:\t\t%s            \n", getReadabilityDescription(getCountIndex()));

		return line1+line2+line3+line4+line5+line6;
	}

	/**
	 * Getting count index.
	 * @return amount of index
	 */
	public double getCountIndex(){
		return 206.835 - 84.6*(totalSyllables*1.0 / wordcount*1.0) - 1.015*(wordcount*1.0 / sentences*1.0);
	}
}
