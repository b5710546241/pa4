package readability;
/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Clock;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * Reveal the result of word counter in GUI.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.07
 */
public class CounterGUI extends JFrame implements Runnable{

	private WordCounter wordcounter;
	private JTextField textField;
	private JTextArea textArea;
	
	/**
	 * Constructor which initialize WordCounter, setting 
	 * JFrame properties, and invoking initComponents.
	 * @param wc is a WordCounter
	 */
	public CounterGUI(WordCounter wc){
		this.wordcounter = wc;
		this.setTitle("Readability by Tuangrat Mungmeerattanaworachot");
		initComponents();
		this.pack();	
	}

	/**
	 * Creating the GUI components.
	 */
	public void initComponents(){
		JPanel pane = new JPanel();
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
		JPanel pane1 = new JPanel();
		pane1.setLayout(new FlowLayout());
		JPanel pane2 = new JPanel();
		pane2.setLayout(new FlowLayout());

		textArea = new JTextArea(10, 50);
		pane2.add(textArea);

		JLabel label1 = new JLabel("    File or URL name: ");
		pane1.add(label1);
		textField = new JTextField(15);
		pane1.add(textField);

		JButton browseButton = new JButton("Browse");
		ActionListener browseButtonListener = new BrowseButtonListener();
		browseButton.addActionListener(browseButtonListener);

		JButton countButton = new JButton(" Count ");
		ActionListener countButtonListener = new CountButtonListener();
		countButton.addActionListener(countButtonListener);

		JButton clearButton = new JButton(" Clear ");
		ActionListener clearButtonListener = new ClearButtonListener();
		clearButton.addActionListener(clearButtonListener);

		pane1.add(browseButton);
		pane1.add(countButton);
		pane1.add(clearButton);

		pane.add(pane1);
		pane.add(pane2);	
		this.add(pane);

		this.pack();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	class BrowseButtonListener implements ActionListener {
		public void actionPerformed( ActionEvent evt ) {
			SimpleFileChooser filechooser = new SimpleFileChooser();
			textField.setText(filechooser.getFileName());
		}
	}

	class CountButtonListener implements ActionListener {
		public void actionPerformed( ActionEvent evt ) {
			if(textField.getText().length() == 0)
				JOptionPane.showMessageDialog(null, "Please choose a file or input URL.", null, JOptionPane.WARNING_MESSAGE);
			else {
				WordCounter wordcounter = new WordCounter();
				wordcounter.count(textField.getText());
				String output = wordcounter.getResult(textField.getText());
				textArea.setText(output);
			}
		}
	}

	class ClearButtonListener implements ActionListener {
		public void actionPerformed( ActionEvent evt ) {
			textField.setText("");
			textArea.setText("");
		}
	}

	/**
	 * Main method for revealing word counter in GUI.
	 * @param args is not used
	 */
	public static void main(String[] args){
		WordCounter wordcounter = new WordCounter();
		CounterGUI counterGUI = new CounterGUI(wordcounter);
	}

	@Override
	public void run() {
		this.pack();
		this.setVisible(true);
	}

}
