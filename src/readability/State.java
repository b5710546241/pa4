/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */
package readability;

/**
 * Abstract class that used for checking a kind of a character.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.07
 */
public abstract class State {
	static State STARTSTATE = new StartState();
	static State CONSONANTSTATE = new ConsonantState();
	static State VOWELSTATE = new VowelState();
	static State ESTATE = new EState();
	static State DASHSTATE = new DashState();
	static State NONWORDSTATE = new NonwordState();
	
	/** Checking each states of a character. 
	 *  @param counter is a wordcounter
	 *	@param c is the character of the word
	 */
	public abstract void handleChar(WordCounter counter, char c);
	
	/** Counting an amount of vowel syllable in each words. 
	 *  @param counter is a wordcounter
	 */
	public abstract void enterState(WordCounter counter);
	
	/**
	 * Checking a letter.
	 * @param c is a character
	 * @return true if c is letter, otherwise return false
	 */
	public boolean isLetter(char c) {
		return Character.isLetter(c);
	} 

	/**
	 * Checking a vowel.
	 * @param c is the character of the word
	 * @return true if the index of the first occurrence 
	 * 		   of the character in the character sequence 
	 * 		   is greater or equals 0, otherwise return -1
	 */
	public boolean isVowel(char c) {
		return ("AEIOUYaeiouy".indexOf(c) >= 0);
	}
	
	/**
	 * Checking a character "E or e".
	 * @param c is the character of the word
	 * @return true if the index of the first occurrence 
	 * 		   of the character in the character sequence 
	 * 		   is greater or equals 0, otherwise return -1
	 */
	public boolean isE(char c) {
		return ("Ee".indexOf(c) >= 0);
	}

	/**
	 * Checking hyphen (dash "-").
	 * @param c is the character of the word
	 * @return true if the index of the first occurrence 
	 * 		   of the character in the character sequence 
	 * 		   is greater or equals 0, otherwise return -1
	 */
	public boolean isDash(char c) {
		return ("-".indexOf(c) >= 0);
	}
	
}
