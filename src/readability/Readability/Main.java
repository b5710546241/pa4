package readability.Readability;
/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import readability.CounterGUI;
import readability.WordCounter;

/**
 * Reads all the words from a URL or File.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.03.24
 */
public class Main {
	
	/**
	 * Reading the words from a URL or File.
	 * Printing the results on the console or command line.
	 * @param args is used for storing URL
	 */
	public static void main(String[] args) {
		
		WordCounter wordcounter = new WordCounter();
		
		if(args.length>0) {
			wordcounter.count(args[0]);
			System.out.print(wordcounter.getResult(args[0]));
		}
		
		else {
			CounterGUI counterGUI = new CounterGUI(wordcounter);
			counterGUI.run();
		}
		
		
	}
}
