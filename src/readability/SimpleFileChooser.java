package readability;
/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

import javax.swing.JFileChooser;
import javax.swing.JFrame;

/**
 * Choosing file form the device.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.07
 */
public class SimpleFileChooser extends JFrame{

	/**
	 * Getting file name.
	 * @return file name
	 */
	public String getFileName(){
		JFrame frame = new JFrame();
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.showOpenDialog(frame);
		return fileChooser.getSelectedFile().getAbsolutePath();
	}
	
}
