package readability;
/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

/**
 * WordCounter will enter this state if the character is consonant.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.07
 */
public class ConsonantState extends State {

	@Override
	public void handleChar(WordCounter counter, char c) {
		if(isE(c)) counter.setState(ESTATE);
		else if(isVowel(c)) counter.setState(VOWELSTATE);
		else if(isLetter(c)) counter.setState(CONSONANTSTATE);
		else if(isDash(c)) counter.setState(DASHSTATE);
		else counter.setState(NONWORDSTATE);
	}

	@Override
	public void enterState(WordCounter counter) {

	}


}
